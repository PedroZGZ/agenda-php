<?php
$dbAgenda ="contactos";

// Tamaño de los campos en la tabla
define("TAM_NOMBRE",     40);  // Tamaño del campo Nombre
define("TAM_APELLIDOS",  60);  // Tamaño del campo Apellidos
define("TAM_TELEFONO",   10);  // Tamaño del campo Teléfono
define("TAM_CORREO",     50);  // Tamaño del campo Correo

// Consulta de creación de tabla en SQLite
$consulta = "CREATE TABLE $dbAgenda (
    id INTEGER PRIMARY KEY,
    nombre VARCHAR(" . TAM_NOMBRE . "),
    apellidos VARCHAR(" . TAM_APELLIDOS . "), 
    telefono VARCHAR(" . TAM_TELEFONO . "),
    correo VARCHAR(" . TAM_CORREO . ")
    )";
?>



<?php
// test de acceso a sqlite
try {
	// crear bases de datos
	$conn = new PDO('sqlite:Agenda.sqlite3');
	//$conn = new PDO('sqlite::memory:'); //en memoria
	// creación de la tabla
	$conn->exec($consulta);
} catch(PDOException $e){
	echo $e->getMessage();
}

// cierra conexion
$conn = null;

?>