<?php
include("encabezado.php");

echo $encabezado1;
echo "Listar";
echo $encabezado2;
echo "Listar";
echo $encabezado3;

$db = new PDO('sqlite:Agenda.sqlite3');

$patron = isset($_GET['patron']) ? $_GET['patron']: "";
$orden = isset($_GET['orden']) ? $_GET['orden']: "";
switch ($patron) {
    case "nombre":
        if($orden=="asc"){
			$resultados = $db->query("SELECT * FROM contactos ORDER BY ".$patron." DESC");
		}else{
			$resultados = $db->query("SELECT * FROM contactos ORDER BY ".$patron." ASC");
		}
        break;
    case "apellidos":
        if($orden=="asc"){
			$resultados = $db->query("SELECT * FROM contactos ORDER BY ".$patron." DESC");
		}else{
			$resultados = $db->query("SELECT * FROM contactos ORDER BY ".$patron." ASC");
		}
        break;
	case "telefono":
        if($orden=="asc"){
			$resultados = $db->query("SELECT * FROM contactos ORDER BY ".$patron." DESC");
		}else{
			$resultados = $db->query("SELECT * FROM contactos ORDER BY ".$patron." ASC");
		}
        break;
	case "correo":
        if($orden=="asc"){
			$resultados = $db->query("SELECT * FROM contactos ORDER BY ".$patron." DESC");
		}else{
			$resultados = $db->query("SELECT * FROM contactos ORDER BY ".$patron." ASC");
		}
        break;
}

if ($patron==null){
	$resultados = $db->query('SELECT * FROM contactos');
}
echo "<table border='1'>";

if(($orden==null) OR ($patron==null) OR ($orden=="desc")){
	echo "<tr class='color2'>
		<th><a href='listar.php?orden=asc&patron=nombre'><img src='Imagenes/flecha_abajo.png' height='8' width='8'> Nombre <img src='Imagenes/flecha_arriba.png' height='8' width='8'></a></th>
		<th><a href='listar.php?orden=asc&patron=apellidos'><img src='Imagenes/flecha_abajo.png' height='8' width='8'> Apellidos <img src='Imagenes/flecha_arriba.png' height='8' width='8'></a></th>
		<th><a href='listar.php?orden=asc&patron=telefono'><img src='Imagenes/flecha_abajo.png' height='8' width='8'> Tel�fono <img src='Imagenes/flecha_arriba.png' height='8' width='8'></a></th>
		<th><a href='listar.php?orden=asc&patron=correo'><img src='Imagenes/flecha_abajo.png' height='8' width='8'> Correo <img src='Imagenes/flecha_arriba.png' height='8' width='8'></a></th>
	</tr>";
}else{
	echo "<tr class='color2'>
		<th><a href='listar.php?orden=desc&patron=nombre'><img src='Imagenes/flecha_abajo.png' height='8' width='8'> Nombre <img src='Imagenes/flecha_arriba.png' height='8' width='8'></a></th>
		<th><a href='listar.php?orden=desc&patron=apellidos'><img src='Imagenes/flecha_abajo.png' height='8' width='8'> Apellidos <img src='Imagenes/flecha_arriba.png' height='8' width='8'></a></th>
		<th><a href='listar.php?orden=desc&patron=telefono'><img src='Imagenes/flecha_abajo.png' height='8' width='8'> Tel�fono <img src='Imagenes/flecha_arriba.png' height='8' width='8'></a></th>
		<th><a href='listar.php?orden=desc&patron=correo'><img src='Imagenes/flecha_abajo.png' height='8' width='8'> Correo <img src='Imagenes/flecha_arriba.png' height='8' width='8'></a></th>
	</tr>";
}
$color=1;
foreach($resultados as $row) {
	extract($row);
	if(($color==null) OR ($color==1)){
		echo "<tr class='color1'>";
		$color=2;
	}else{
		echo "<tr class='color2'>";
		$color=1;
	}
	echo "<td>{$nombre}</td>";
	echo "<td>{$apellidos}</td>";
	echo "<td>{$telefono}</td>";
	echo "<td>{$correo}</td>";
	echo "</tr>";
}
echo "</table>";
?>